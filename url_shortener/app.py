import os
import time

from flask import Flask
from flask_cors import CORS

from url_shortener.routes import bp as routes_bp
from url_shortener.client import bp as client_bp
from url_shortener.db import db

MAX_CONNECTION_RETRIES = 30
DBUSER = 'gott'
DBPASS = 'changeme'
DBHOST = 'db'
DBPORT = '5432'
DBNAME = 'testdb'

DATABASE_URI = f'postgresql+psycopg2://{DBUSER}:{DBPASS}@{DBHOST}:{DBPORT}/{DBNAME}'

def create_app():
    app = Flask('url_shortener', static_folder=None)
    app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URI
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    # register blueprints
    app.register_blueprint(client_bp)
    app.register_blueprint(routes_bp)

    # enable CORS for simplicity
    CORS(app)

    # wait for database to be ready
    for _ in range(MAX_CONNECTION_RETRIES):
        try:
            # init database
            db.init_app(app)
            db.create_all(app=app)
            break
        except Exception as e:
            # wait 5 seconds before retrying
            time.sleep(5)

    return app
