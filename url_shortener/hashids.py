from hashids import Hashids

# IMPORTANT: This should never be changed or all the previously generated urls will become invalid
SALT = 'tfw u r salty of salt'
ALPHABET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
# Around this number we start getting a 5-length encoded string
OFFSET = 39310

hashids = Hashids(SALT, alphabet=ALPHABET)

def encode(id):
    "Encode integer into a string of alphabets of 5+ characters"
    # Add some offset to the integer so that the first ids are not too small to be readable
    return hashids.encode(OFFSET + int(id))

def decode(key):
    "Decode the string back to an integer"
    decoded_numbers = hashids.decode(key)
    if not decoded_numbers:
        return None
    return decoded_numbers[0] - OFFSET
