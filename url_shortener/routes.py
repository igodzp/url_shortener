import validators

from flask import Blueprint
from flask import request
from flask import redirect
from flask import abort
from flask.json import jsonify
from urllib.parse import urlparse

from url_shortener.hashids import encode
from url_shortener.hashids import decode
from url_shortener.db import db
from url_shortener.db import Url

bp = Blueprint('api', __name__)

def format_url(url):
    if not urlparse(url).scheme and not url.startswith('//'):
        # appeding double slash so that urlparse can recognize netloc
        url = '//' + url

    return urlparse(url, scheme='http').geturl()

@bp.route('/<encoded_id>')
def get_url(encoded_id):
    "get url in the database from the encoded_id"
    url_id = decode(encoded_id)

    if not url_id:
        abort(404)

    url = Url.query.get(url_id)

    if not url:
        abort(404)

    return redirect(url.url, code=301)


@bp.route('/shorten_url', methods=['POST'])
def shorten_url():
    """insert the url and its generated short url into the database"""
    try:
        data = request.get_json(force=True)
        url = format_url(data.get('url'))

        if not validators.url(url):
            return f'"{url}" is not a valid url', 400

        new_url = Url(url=url)
        db.session.add(new_url)
        db.session.commit()

        res = jsonify(shortened_url=f'{request.url_root}{encode(new_url.id)}')
        return res, 201

    except Exception as e:
        print(e)
        return f'There is an error processing your request', 500
