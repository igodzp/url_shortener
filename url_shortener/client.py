from flask import Blueprint
from flask import send_from_directory

bp = Blueprint('client', __name__)

@bp.route('/static/<path:path>')
def serve_static(path):
    return send_from_directory('../build/static', path)

@bp.route('/', defaults={'path': ''})
def serve(path):
    return send_from_directory('../build', 'index.html')
