FROM node:8.12.0-alpine as build-client
WORKDIR /app
COPY . /app
RUN yarn
RUN yarn build

FROM python:3.6
RUN apt-get update
COPY --from=build-client /app /flask_app
WORKDIR /flask_app
RUN pip install --no-cache-dir -r requirements.txt
ENV FLASK_ENV="docker"
EXPOSE 5000
