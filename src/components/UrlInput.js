import React from "react";
import styled, { keyframes } from "styled-components";
import { Box, Flex, Link, Text } from "rebass";

const fadeIn = keyframes`
  0%   {opacity: 0}
  50%  {transform: scale(0.8);}
  100% {opacity: 1}
`

const InputBox = styled.div`
  background: #fff;
  box-shadow: 0 6px 8px rgba(102, 119, 136, 0.03),
    0 1px 2px rgba(102, 119, 136, 0.3);
  display: flex;
  height: 40px;
  align-items: center;
  border-radius: 6px;
  min-width: 35vw;
  padding: 12px;
  transition: all 0.2s;
  border: 1px solid white;

  ${p => p.hasValue && "border: 1px solid #4C64FD;"} ${p =>
    p.hasError &&
    `
    border: 1px solid #F15C4F;
    > input {
      color: #F15C4F
    }
  `};
`;

const Input = styled.input`
  background: none;
  border: none;
  font-size: 18px;
  padding: 12px;
  padding-bottom: 14px;
  outline: None;
  flex: 1;
  min-width: 300px;

  ${p => p.value.length && "font-weight: 500;"};
`;

const Button = styled.button`
  appearance: none;;
  border: 0;
  background: #4C64FD;
  color: white;
  font-size: 18px;
  padding: 10px 15px;
  border-radius: 4px;
  font-weight: 500;
  cursor: pointer;
  transition: all 0.2s;

  &:hover {
    opacity: 0.8;
  }

  &[disabled] {
    opacity: 0.4;
    cursor: default;
  }

  ${p => p.hasError && `
    background: #F15C4F;
  `}
`;

const AlertBox = styled(Box).attrs({
  p: 3,
  mb: 3,
  bg: "#DAECD2"
})`
  animation: ${fadeIn} 0.2s linear;
  border-radius: 6px;
`

const CopyButton = styled(Button)`
  width: 100%;
  font-size: 14px;
  padding: 6px;
  margin-top: 6px;
`

class UrlInput extends React.Component {
  state = {
    url: "",
    lastShortenedUrl: null,
    loading: false,
    error: false,
    linkCopied: false,
  };

  // Sneakily insert a textarea and execute a copy command before removing it
  copyToClipboard = () => {
    const textField = document.createElement('textarea')
    textField.innerText = this.state.lastShortenedUrl
    document.body.appendChild(textField)
    textField.select()
    document.execCommand('copy')
    textField.remove()
    this.setState({ linkCopied: true })
  }

  handleChange = e => {
    this.setState({ url: e.target.value.trim(), error: false });
  };

  handleSubmit = async () => {
    const { url } = this.state;
    if (!url) return;

    this.setState({ loading: true, lastShortenedUrl: null });

    try {
      const resp = await fetch("http://localhost:5000/shorten_url", {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        headers: {
          "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({ url })
      });
      const data = await resp.json();
      this.setState({
        url: "",
        lastShortenedUrl: data.shortened_url,
        error: false,
        linkCopied: false,
      });
    } catch (e) {
      console.error(e);
      this.setState({ error: true });
    } finally {
      this.setState({ loading: false });
    }
  };

  render() {
    const { url, loading, error, lastShortenedUrl, linkCopied } = this.state;
    return (
      <Box css={{ opacity: loading ? "0.3" : 1 }}>
        {lastShortenedUrl && (
          <AlertBox>
            <Flex justifyContent="center" alignItem="center">
              <Text fontSize={16} fontWeight={500} color="#308941" mr={1}>
                Success!
              </Text>{" "}
              <Text fontWeight={500} mb={1} fontSize={16}>
                Your shortened url is
              </Text>
            </Flex>
            <Link href={lastShortenedUrl} target="_blank" rel="noopener">
              {lastShortenedUrl}
            </Link>
            <Box mt={2}>
              {linkCopied
                ? <Text fontSize={14}>Copied to clipboard</Text>
                : <CopyButton onClick={this.copyToClipboard}>Copy link</CopyButton>
              }
            </Box>
          </AlertBox>
        )}
        <InputBox hasValue={url.length > 0} hasError={error}>
          <Input
            value={url}
            placeholder="Example: www.google.com"
            onChange={this.handleChange}
            onKeyPress={e => e.key === 'Enter' && this.handleSubmit()}
          />
          <Button hasError={error} onClick={this.handleSubmit} disabled={loading || !url || error}>
            Shorten
          </Button>
        </InputBox>
          <AlertBox bg="">
            {error &&
            <Text color="red" fontSize={16}>Please enter a valid url!</Text>
          }
          </AlertBox>
      </Box>
    );
  }
}

export default UrlInput;
