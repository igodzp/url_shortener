import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import UrlInput from './components/UrlInput';
import { Text } from 'rebass'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div className="App-logo">✂️</div>
          <Text mb={3} fontSize={36}>
            Get your URL shortened in seconds!
          </Text>
          <UrlInput/>
        </header>
      </div>
    );
  }
}

export default App;
